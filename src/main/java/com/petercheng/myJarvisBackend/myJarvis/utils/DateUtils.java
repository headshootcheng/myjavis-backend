package com.petercheng.myJarvisBackend.myJarvis.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static Date addSecondToDate(Date date, Integer second){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, second);
        return calendar.getTime();
    }
    public static Date convertFromLocalDateTimeToDate(LocalDateTime localDateTime){
    	return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
