package com.petercheng.myJarvisBackend.myJarvis.utils;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

import static org.quartz.TriggerBuilder.newTrigger;

public class ScheduleUtils {

    public static void scheduleJobWithTrigger(final String jobName, String jobGroup, String dailyExecutionSchedule, LocalDateTime oneOffExecutionSchedule, JobDetail jobDetail)
            throws Exception
        {
            final Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            Trigger trigger = null;
            if(oneOffExecutionSchedule != null){
	            trigger = TriggerBuilder.newTrigger()
		              .withIdentity(jobGroup, jobName)
		              .startAt(DateUtils.convertFromLocalDateTimeToDate(oneOffExecutionSchedule))
		              .withSchedule(SimpleScheduleBuilder.repeatSecondlyForTotalCount(1))
		              .build();
            }
            if(StringUtils.hasText(dailyExecutionSchedule) && dailyExecutionSchedule != null)
            {
                trigger = newTrigger().withIdentity(jobGroup, jobName)
                      .withSchedule(CronScheduleBuilder.cronSchedule(dailyExecutionSchedule)).build();
            }
	        // Tell quartz to schedule the job using our trigger
	        scheduler.scheduleJob(jobDetail, trigger);
	        if(!scheduler.isStarted())
		        scheduler.start();
        }
}
