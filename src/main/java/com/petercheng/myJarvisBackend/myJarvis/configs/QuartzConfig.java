package com.petercheng.myJarvisBackend.myJarvis.configs;


import com.petercheng.myJarvisBackend.myJarvis.repositories.TaskRepository;
import com.petercheng.myJarvisBackend.myJarvis.tasks.TestTask;
import com.petercheng.myJarvisBackend.myJarvis.utils.DateUtils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;


@Configuration
public class QuartzConfig
{

	@Autowired
	private TaskRepository taskRepository;

	@Bean
	public Scheduler scheduler() throws SchedulerException {
		final Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		Map<JobDetail, Set<? extends Trigger>> triggerMap = new HashMap<>();
		taskRepository.findAll().forEach(task -> {
			JobDetail jobDetail = JobBuilder.newJob(TestTask.class).withIdentity(task.getName()+task.getId()).storeDurably().build();
			jobDetail.getJobDataMap().put("name", task.getName());
			Trigger trigger = null;
			if(task.getDailySchedule() != null)
			{
				trigger = TriggerBuilder.newTrigger()
					  .withIdentity("Trigger" + task.getName())
					  .withSchedule(CronScheduleBuilder.cronSchedule(task.getDailySchedule()))
					  .build();
			}
			else{
				 trigger = TriggerBuilder.newTrigger()
					  .withIdentity("Trigger" + task.getName())
					  .startAt(DateUtils.convertFromLocalDateTimeToDate(task.getOneOffExecution()))
					  .withSchedule(SimpleScheduleBuilder.repeatSecondlyForTotalCount(1))
					  .build();
			}
			triggerMap.put(jobDetail, Collections.singleton(trigger));
		});
		scheduler.scheduleJobs(triggerMap, false);
		scheduler.start();
		return scheduler;
	}
}

