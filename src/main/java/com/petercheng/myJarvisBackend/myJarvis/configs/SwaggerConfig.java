package com.petercheng.myJarvisBackend.myJarvis.configs;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class SwaggerConfig implements WebMvcConfigurer
{
	@Value("${springdoc.swagger-ui.path:#{null}}")
	private String springdocSwaggerUiPath;

	@Bean
	public OpenAPI reviewApiOpenApi()
	{
		final Info info = new Info() //
			  .title("My Javis") //
			  .description("The API Endpoint of my Javis") //
			  .version("v1");

		return new OpenAPI() //
			  .components(new Components().addSecuritySchemes(
					"BEARER_TOKEN",
					new SecurityScheme()
						  .description("Please enter your SSO token. For SSO token, add the \"sso-\" prefix.")
						  .type(SecurityScheme.Type.HTTP)
						  .scheme("bearer")
						  .in(SecurityScheme.In.HEADER)
						  .name("Authorization"))) //
			  .info(info);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry)
	{
		if (StringUtils.isNotEmpty(springdocSwaggerUiPath))
		{
			registry.addRedirectViewController("/docs", springdocSwaggerUiPath);
			registry.addRedirectViewController("/docs/", springdocSwaggerUiPath);
		}
	}
}
