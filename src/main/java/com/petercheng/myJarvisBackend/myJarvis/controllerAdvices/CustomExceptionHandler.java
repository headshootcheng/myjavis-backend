package com.petercheng.myJarvisBackend.myJarvis.controllerAdvices;

import com.petercheng.myJarvisBackend.myJarvis.exceptions.AuthorizationErrorException;
import com.petercheng.myJarvisBackend.myJarvis.exceptions.ValidationErrorException;
import com.petercheng.myJarvisBackend.myJarvis.models.AuthMessageResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomExceptionHandler
{
	@ExceptionHandler({ ValidationErrorException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<AuthMessageResponse> handleValidationException(Exception exception){
		AuthMessageResponse authMessageResponse = new AuthMessageResponse();
		authMessageResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		authMessageResponse.setMessage(exception.getMessage());
		authMessageResponse.setTimeStamp(System.currentTimeMillis());
		authMessageResponse.setError(true);
		return new ResponseEntity<>(authMessageResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ AuthorizationErrorException.class})
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ResponseEntity<AuthMessageResponse> handleAuthorizationException(Exception exception){
		AuthMessageResponse authMessageResponse = new AuthMessageResponse();
		authMessageResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
		authMessageResponse.setMessage(exception.getMessage());
		authMessageResponse.setTimeStamp(System.currentTimeMillis());
		authMessageResponse.setError(true);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).contentType(MediaType.APPLICATION_JSON).body(authMessageResponse);
	}
}
