package com.petercheng.myJarvisBackend.myJarvis.repositories;

import com.petercheng.myJarvisBackend.myJarvis.models.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long>
{
	//Optional<Task> findById(Long id);
}
