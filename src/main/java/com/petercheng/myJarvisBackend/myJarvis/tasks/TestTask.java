package com.petercheng.myJarvisBackend.myJarvis.tasks;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;


public class TestTask extends QuartzJobBean
{
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException
	{
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("Task--->"+  dataMap.getString("name")+" Date:  " +  sdf.format(new Date()));
	}
}
