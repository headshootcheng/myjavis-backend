package com.petercheng.myJarvisBackend.myJarvis.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.petercheng.myJarvisBackend.myJarvis.exceptions.ValidationErrorException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class DateTimeSerializer extends JsonSerializer<LocalDateTime>
{
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
	@Override
	public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
	{
		try{
			jsonGenerator.writeString(localDateTime.format(formatter));
		}
		catch(Exception e){
			throw  new ValidationErrorException("Wrong Date Format");
		}
	}
}
