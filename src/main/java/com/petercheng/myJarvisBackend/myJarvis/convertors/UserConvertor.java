package com.petercheng.myJarvisBackend.myJarvis.convertors;

import com.petercheng.myJarvisBackend.myJarvis.models.entity.Account;
import com.petercheng.myJarvisBackend.myJarvis.models.response.UserInfo;

import java.util.List;
import java.util.stream.Collectors;


public class UserConvertor {
	public static UserInfo fromAccountToUserInfo(Account account){
		UserInfo userInfo = new UserInfo();
		userInfo.setId(account.getUserId());
		userInfo.setUsername(account.getUsername());
		userInfo.setEmail(account.getEmail());
		List<String> roleList = account.getRoleList().stream().map(role -> role.getAuthority().toString()).collect(Collectors.toList());
		userInfo.setRoles(roleList);
		return userInfo;
	}
}

