package com.petercheng.myJarvisBackend.myJarvis.services;

import com.petercheng.myJarvisBackend.myJarvis.models.entity.Task;
import com.petercheng.myJarvisBackend.myJarvis.models.request.TaskCreateRequest;
import org.quartz.SchedulerException;

public interface TaskService {
    Task createNewTask(TaskCreateRequest task, Long userId) throws Exception;

}
