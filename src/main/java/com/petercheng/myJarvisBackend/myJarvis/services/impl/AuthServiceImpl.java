package com.petercheng.myJarvisBackend.myJarvis.services.impl;


import com.petercheng.myJarvisBackend.myJarvis.enums.RoleEnum;
import com.petercheng.myJarvisBackend.myJarvis.exceptions.ValidationErrorException;
import com.petercheng.myJarvisBackend.myJarvis.models.entity.Account;
import com.petercheng.myJarvisBackend.myJarvis.models.entity.Role;
import com.petercheng.myJarvisBackend.myJarvis.models.request.LoginRequest;
import com.petercheng.myJarvisBackend.myJarvis.models.request.SignUpRequest;
import com.petercheng.myJarvisBackend.myJarvis.models.response.JwtResponse;
import com.petercheng.myJarvisBackend.myJarvis.models.response.UserInfo;
import com.petercheng.myJarvisBackend.myJarvis.repositories.AccountRepository;
import com.petercheng.myJarvisBackend.myJarvis.services.AuthService;
import com.petercheng.myJarvisBackend.myJarvis.services.security.UserDetailsImpl;
import com.petercheng.myJarvisBackend.myJarvis.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService
{
	private static Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private JwtUtils jwtUtils;

	@Override
	@Transactional
	public void signUpUser(SignUpRequest signupRequest){
		if(accountRepository.existsAccountByUsername(signupRequest.getUsername())){
			throw new ValidationErrorException("usernameExistedError");
		}
		if(accountRepository.existsAccountByEmail(signupRequest.getEmail())){
			throw new ValidationErrorException("emailExistedError");
		}
		Account account = new Account(signupRequest.getUsername(), signupRequest.getEmail(), encoder.encode(signupRequest.getPassword()));
		account.setRoleList(Collections.singletonList(new Role(account, RoleEnum.ROLE_USER)));
		accountRepository.save(account);
	}

	@Override
	@Transactional
	public void signUpAdmin(SignUpRequest signupRequest){
		if(accountRepository.existsAccountByUsername(signupRequest.getUsername())){
			throw new ValidationErrorException("Error: Username is already taken ");
		}
		if(accountRepository.existsAccountByEmail(signupRequest.getEmail())){
			throw new ValidationErrorException("Error: Email is already taken ");
		}
		Account account = new Account(signupRequest.getUsername(), signupRequest.getEmail(), encoder.encode(signupRequest.getPassword()));
		List<Role> roleList = new ArrayList<>();
		roleList.add(new Role(account, RoleEnum.ROLE_USER));
		roleList.add(new Role(account, RoleEnum.ROLE_ADMIN));
		account.setRoleList(roleList);
		accountRepository.save(account);
	}


	@Override
	@Transactional
	public JwtResponse userLogin(LoginRequest loginRequest){
		Authentication authentication = authenticationManager.authenticate(
			  new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
			  .map(item -> item.getAuthority())
			  .collect(Collectors.toList());
		UserInfo userInfo = new UserInfo(userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles);

		return new JwtResponse(jwt, userInfo);
	}


}

