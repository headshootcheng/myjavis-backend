package com.petercheng.myJarvisBackend.myJarvis.services;

import com.petercheng.myJarvisBackend.myJarvis.models.response.UserInfo;

import java.util.List;


public interface UserService
{
	List<UserInfo> getUserList();

	UserInfo getUserInfoById(Long id);

	UserInfo getUserInfoFromUsername(String username);
}
