package com.petercheng.myJarvisBackend.myJarvis.services.impl;

import com.petercheng.myJarvisBackend.myJarvis.convertors.UserConvertor;
import com.petercheng.myJarvisBackend.myJarvis.exceptions.ValidationErrorException;
import com.petercheng.myJarvisBackend.myJarvis.models.entity.Account;
import com.petercheng.myJarvisBackend.myJarvis.models.response.UserInfo;
import com.petercheng.myJarvisBackend.myJarvis.repositories.AccountRepository;
import com.petercheng.myJarvisBackend.myJarvis.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService
{

	@Autowired
	private AccountRepository accountRepository;

	@Override
	public List<UserInfo> getUserList(){
		List<Account> accountList = accountRepository.findAll();
		List<UserInfo> userInfoList = accountList.stream().map(account -> UserConvertor.fromAccountToUserInfo(account)).collect(Collectors.toList());
		return userInfoList;
	}

	@Override
	public UserInfo getUserInfoById(Long id){
		Account account = accountRepository.findById(id).orElse(null);
		UserInfo userInfo = UserConvertor.fromAccountToUserInfo(account);
		return userInfo;
	}

	@Override
	public UserInfo getUserInfoFromUsername(String username){
		Account account = accountRepository.findAccountByUsername(username).orElseThrow(() -> new ValidationErrorException("User is not found !!!"));
		UserInfo userInfo = UserConvertor.fromAccountToUserInfo(account);
		return userInfo;
	}
}
