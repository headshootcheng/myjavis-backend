package com.petercheng.myJarvisBackend.myJarvis.services.impl;

import com.petercheng.myJarvisBackend.myJarvis.models.entity.Task;
import com.petercheng.myJarvisBackend.myJarvis.models.request.TaskCreateRequest;
import com.petercheng.myJarvisBackend.myJarvis.repositories.TaskRepository;
import com.petercheng.myJarvisBackend.myJarvis.services.TaskService;
import com.petercheng.myJarvisBackend.myJarvis.tasks.TestTask;
import com.petercheng.myJarvisBackend.myJarvis.utils.ScheduleUtils;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Task createNewTask(TaskCreateRequest taskCreateRequest, Long userId) throws Exception {
        Task task = new Task(taskCreateRequest.getTaskName(), taskCreateRequest.getTaskDetail(), taskCreateRequest.getTaskDate()
                ,  userId, LocalDateTime.now());
        if(taskCreateRequest.getOneOffExecutionTime() != null && taskCreateRequest.getDailyExecutionTime() != null){
        	throw new Exception("Cannot accept 2 types of reminder task at the same time");
        }
        if(taskCreateRequest.getOneOffExecutionTime() != null){
        	task.setOneOffExecution(taskCreateRequest.getOneOffExecutionTime());
        }
        if(taskCreateRequest.getDailyExecutionTime() != null){
        	task.setDailySchedule(taskCreateRequest.getDailyExecutionTime());
        }
	    taskRepository.save(task);
	    JobDetail jobDetail = JobBuilder.newJob(TestTask.class).withIdentity(task.getName() + task.getId()).storeDurably().build();
	    jobDetail.getJobDataMap().put("name", task.getName());
	    ScheduleUtils.scheduleJobWithTrigger(task.getName(), "reminderTask", task.getDailySchedule(), task.getOneOffExecution(), jobDetail);
        return task;
    }

}
