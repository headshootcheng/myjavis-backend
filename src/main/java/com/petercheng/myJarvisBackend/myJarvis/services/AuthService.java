package com.petercheng.myJarvisBackend.myJarvis.services;

import com.petercheng.myJarvisBackend.myJarvis.models.request.LoginRequest;
import com.petercheng.myJarvisBackend.myJarvis.models.request.SignUpRequest;
import com.petercheng.myJarvisBackend.myJarvis.models.response.JwtResponse;

import javax.transaction.Transactional;


public interface AuthService
{
	@Transactional void signUpUser(SignUpRequest signupRequest);

	@Transactional void signUpAdmin(SignUpRequest signupRequest);

	@Transactional JwtResponse userLogin(LoginRequest loginRequest);
}
