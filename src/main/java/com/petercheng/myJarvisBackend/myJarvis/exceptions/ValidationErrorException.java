package com.petercheng.myJarvisBackend.myJarvis.exceptions;

public class ValidationErrorException extends RuntimeException{
	public ValidationErrorException(String message) {
		super(message);
	}
}
