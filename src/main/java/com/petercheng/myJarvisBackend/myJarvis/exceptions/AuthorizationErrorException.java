package com.petercheng.myJarvisBackend.myJarvis.exceptions;

public class AuthorizationErrorException extends RuntimeException{
	public AuthorizationErrorException(String message) {
		super(message);
	}
}
