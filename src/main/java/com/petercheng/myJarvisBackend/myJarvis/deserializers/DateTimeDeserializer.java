package com.petercheng.myJarvisBackend.myJarvis.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.petercheng.myJarvisBackend.myJarvis.exceptions.ValidationErrorException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class DateTimeDeserializer extends JsonDeserializer<LocalDateTime>
{
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/M/dd H:m");
	@Override
	public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
	{
		try
		{
			String dateTimeStr = jsonParser.getText();
			return LocalDateTime.parse(dateTimeStr, formatter);
		}
		catch (Exception e){
			throw new ValidationErrorException("Wrong Date Format !!!");
		}
	}
}
