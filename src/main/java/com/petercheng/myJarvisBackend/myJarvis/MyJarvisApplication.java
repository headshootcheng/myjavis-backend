package com.petercheng.myJarvisBackend.myJarvis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


@SpringBootApplication
@ServletComponentScan
public class MyJarvisApplication {
	public static void main(String[] args) {
		SpringApplication.run(MyJarvisApplication.class, args);
	}
}
