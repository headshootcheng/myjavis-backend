package com.petercheng.myJarvisBackend.myJarvis.controllers;

import com.petercheng.myJarvisBackend.myJarvis.exceptions.ValidationErrorException;
import com.petercheng.myJarvisBackend.myJarvis.models.entity.Task;
import com.petercheng.myJarvisBackend.myJarvis.models.request.TaskCreateRequest;
import com.petercheng.myJarvisBackend.myJarvis.models.response.TaskCreateResponse;
import com.petercheng.myJarvisBackend.myJarvis.repositories.TaskRepository;
import com.petercheng.myJarvisBackend.myJarvis.services.TaskService;
import com.petercheng.myJarvisBackend.myJarvis.utils.JwtUtils;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@RestController
@RequestMapping("/api/task")
public class TaskController
{
	@Autowired
	private TaskService taskService;

	@Autowired
	private JwtUtils jwtUtils;

	private static Logger logger = LoggerFactory.getLogger(TaskController.class);

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}


	@PostMapping( "/create")
	public ResponseEntity<TaskCreateResponse> createNewTask(
		  @RequestBody @Valid TaskCreateRequest taskCreateRequest,
		  BindingResult theBindingResult,
		  HttpServletRequest httpServletRequest
		  )  {
		String token = jwtUtils.parseJwt(httpServletRequest);
		Long userId = jwtUtils.getUserIdFromJwtToken(token);
		if(theBindingResult.hasErrors()){
			throw new ValidationErrorException(theBindingResult.getFieldError().getDefaultMessage());
		}
		try {
			Task task = taskService.createNewTask(taskCreateRequest, userId);
			return ResponseEntity.ok(new TaskCreateResponse(true, task));
		}
		catch (Exception e){
			logger.info(e.getMessage());
			throw new ValidationErrorException(e.getMessage());
		}
	}
}
