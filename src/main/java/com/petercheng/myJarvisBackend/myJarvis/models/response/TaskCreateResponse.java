package com.petercheng.myJarvisBackend.myJarvis.models.response;

import com.petercheng.myJarvisBackend.myJarvis.models.entity.Task;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class TaskCreateResponse
{
	private Boolean created;
	private Task task;
}
