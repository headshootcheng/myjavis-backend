package com.petercheng.myJarvisBackend.myJarvis.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.petercheng.myJarvisBackend.myJarvis.deserializers.DateTimeDeserializer;
import com.petercheng.myJarvisBackend.myJarvis.serializers.DateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskCreateRequest
{
	@NotBlank(message = "Task name cannot be empty")
	private String taskName;

	@NotBlank(message = "Task detail cannot be empty")
	private String taskDetail;

	private String dailyExecutionTime;

	@JsonSerialize(using = DateTimeSerializer.class)
	@JsonDeserialize(using = DateTimeDeserializer.class)
	private LocalDateTime oneOffExecutionTime;

	@JsonSerialize(using = DateTimeSerializer.class)
	@JsonDeserialize(using = DateTimeDeserializer.class)
	private LocalDateTime taskDate;

	@Override
	public String toString()
	{
		return "TaskCreateRequest{" +
			  "taskName='" + taskName + '\'' +
			  ", taskDetail='" + taskDetail + '\'' +
			  ", dailyExecutionTime='" + dailyExecutionTime + '\'' +
			  ", oneOffExecutionTime=" + oneOffExecutionTime +
			  ", taskDate=" + taskDate +
			  '}';
	}
}
