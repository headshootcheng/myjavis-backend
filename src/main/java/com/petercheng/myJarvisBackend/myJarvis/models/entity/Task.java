package com.petercheng.myJarvisBackend.myJarvis.models.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.petercheng.myJarvisBackend.myJarvis.deserializers.DateTimeDeserializer;
import com.petercheng.myJarvisBackend.myJarvis.serializers.DateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity(name = "task")
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
public class Task
{
	public Task(String name, String detail, LocalDateTime date, Long userId, LocalDateTime creationDate) {
		this.name = name;
		this.detail = detail;
		this.date = date;
		this.userId = userId;
		this.creationDate = creationDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String detail;

	@JsonSerialize(using = DateTimeSerializer.class)
	@JsonDeserialize(using = DateTimeDeserializer.class)
	private LocalDateTime date;

	@Column(name = "daily_schedule")
	private String dailySchedule;

	@JsonSerialize(using = DateTimeSerializer.class)
	@JsonDeserialize(using = DateTimeDeserializer.class)
	@Column(name = "one_off_execution")
	private LocalDateTime oneOffExecution;

	@Column(name = "user_id")
	private Long userId;

	@JsonSerialize(using = DateTimeSerializer.class)
	@JsonDeserialize(using = DateTimeDeserializer.class)
	@Column(name = "creation_date")
	private LocalDateTime creationDate;

	@Override
	public String toString()
	{
		return "Task{" +
			  "id=" + id +
			  ", name='" + name + '\'' +
			  ", detail='" + detail + '\'' +
			  ", date=" + date +
			  ", dailySchedule='" + dailySchedule + '\'' +
			  ", oneOffExecution=" + oneOffExecution +
			  ", userId=" + userId +
			  ", creationDate=" + creationDate +
			  '}';
	}
}
