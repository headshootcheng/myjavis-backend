package com.petercheng.myJarvisBackend.myJarvis.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
	@NotBlank(message = "Username cannot be empty")
	private String username;

	@NotBlank(message = "Password cannot be empty")
	private String password;

	@Override
	public String toString() {
		return "LoginRequest{" +
			  "username='" + username + '\'' +
			  '}';
	}
}
