package com.petercheng.myJarvisBackend.myJarvis.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthMessageResponse {
	private Integer status;
	private String message;
	private long timeStamp;
	private boolean isError;

}
