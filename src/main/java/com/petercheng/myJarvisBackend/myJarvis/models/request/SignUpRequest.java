package com.petercheng.myJarvisBackend.myJarvis.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {

	@NotBlank(message = "Username cannot be empty")
	private String username;

	@NotBlank(message = "Email cannot be empty")
	@Pattern(regexp = "^.+@([a-z]+\\.)+[a-z]{2,4}$", message = "Wrong Email Format")
	private String email;

	@NotBlank(message = "Password cannot be empty")
	private String password;

	@Override
	public String toString() {
		return "SignupRequest{" +
			  "username='" + username + '\'' +
			  ", email='" + email + '\'' +
			  '}';
	}
}
