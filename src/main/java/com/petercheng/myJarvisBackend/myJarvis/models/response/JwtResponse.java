package com.petercheng.myJarvisBackend.myJarvis.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse {
	private String token;
	private UserInfo userInfo;
}
